import React, { useEffect, useState } from "react";
import { SafeAreaView, StyleSheet, Text } from "react-native";
import { getRandomQuote } from "./src/services/quotes";

export default function App() {
  const [quote, setQuote] = useState<IQuote>();

  useEffect(() => {
    const fetchQuote = async () => {
      const quote = await getRandomQuote();

      setQuote(quote);
    };

    fetchQuote();
  }, []);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Text>{quote?.message}</Text>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({});
