import { Feather } from "@expo/vector-icons";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Task } from "../types/Task";
import IconButton from "./IconButton";

type Props = {
  task: Task;
  onDone: (id: number) => void;
};

const TaskItem = ({ onDone, task }: Props) => {
  const { id, title } = task;

  return (
    <View style={styles.container}>
      <Text>{title}</Text>
      <IconButton onPress={() => onDone(id)}>
        <Feather name="check" size={24} color="#787FF6" />
      </IconButton>
    </View>
  );
};

export default TaskItem;

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 10,
    backgroundColor: "#F3F3F3",
    borderColor: "#E3E3E3",
    borderWidth: 1,
    borderRadius: 10,
    marginBottom: 10,
  },
});
