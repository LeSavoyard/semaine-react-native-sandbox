import React, { FC } from "react";
import { Platform, Pressable, StyleSheet, View } from "react-native";

type Props = {
  onPress: () => void;
};

const IconButton: FC<Props> = ({ children, onPress }) => {
  return (
    <Pressable onPress={onPress}>
      <View style={styles.container}>{children}</View>
    </Pressable>
  );
};

export default IconButton;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#F3F3F3",
    borderRadius: 20,
    justifyContent: "center",
    alignItems: "center",
    padding: 8,
    height: 40,
    width: 40,
  },
});
