import React, { Fragment, useState } from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  useWindowDimensions,
  View,
} from "react-native";
import IconButton from "./IconButton";
import { Feather } from "@expo/vector-icons";

type Props = {
  onAdd: (taskTitle: string) => void;
  label: string;
};

const AddTaskForm = ({ onAdd, label }: Props) => {
  const [value, setValue] = useState("");
  const { width } = useWindowDimensions();

  const handleAddTask = (value: string) => {
    if (value.length) {
      onAdd(value);
      setValue("");
    }
  };

  return (
    <>
      <Text style={styles.label}>{label}</Text>
      <View style={{ ...styles.container, width: width - 40 }}>
        <TextInput
          maxLength={50}
          style={styles.input}
          value={value}
          placeholder="Sortir le chien..."
          onChangeText={(inputValue) => setValue(inputValue)}
        />
        <IconButton onPress={() => handleAddTask(value)}>
          <Feather name="save" size={24} color="#787FF6" />
        </IconButton>
      </View>
    </>
  );
};

export default AddTaskForm;

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
  },
  label: {
    marginBottom: 4,
  },
  input: {
    backgroundColor: "#eee",
    padding: 10,
    borderRadius: 10,
    flex: 1,
    marginRight: 10,
  },
});
