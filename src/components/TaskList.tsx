import React, { useState } from "react";
import { FlatList, StyleSheet, Task, Text, View } from "react-native";
import { TaskStatus, Task } from "../types/Task";
import AddTaskForm from "./AddTaskForm";
import Layout from "./Layout";
import TaskItem from "./TaskItem";

const DATA: Task[] = [
  {
    id: 1,
    status: TaskStatus.TODO,
    title: "faire la vaisselle",
  },
  {
    id: 2,
    status: TaskStatus.TODO,
    title: "faire la lessive",
  },
  {
    id: 3,
    status: TaskStatus.TODO,
    title: "faire à manger",
  },
];

const TaskList = () => {
  const [data, setData] = useState<Task[]>(DATA);

  const onTaskDone = (id: number) => {
    const updatedData = data.filter((task) => task.id !== id);

    setData(updatedData);
  };

  return (
    <Layout title="Mes tâches">
      <FlatList
        data={data}
        renderItem={({ item }) => <TaskItem task={item} onDone={onTaskDone} />}
        keyExtractor={({ id }) => id.toString()}
      />
      <AddTaskForm
        label="Ajouter une tâche"
        onAdd={(taskTitle) =>
          setData([
            ...data,
            {
              id: data.length + 1,
              status: TaskStatus.TODO,
              title: taskTitle,
            },
          ])
        }
      />
    </Layout>
  );
};

export default TaskList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 20,
  },
});
